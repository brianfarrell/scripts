#!/usr/bin/perl
my $startUser = 2;
my $endUser = 402;
my $count = $startUser;
open (STARTFILE, ">/root/pjsua/scripts/startListeners.sh");
open (STOPFILE, ">/root/pjsua/scripts/stopListeners.sh");
while ($count < $endUser) {
        my $callee = sprintf("test%05d",$count);
        open (USERFILE,">/root/pjsua/conf/$callee.conf");
        printf USERFILE "--max-calls=1\n--rtp-port=4%04d\n--local-port=5%04d\n--log-file=/root/pjsua/log/pjsua$count.log\n--log-level=5\n--app-log-level=4\n--color\n--registrar=sip:k.siptest.dev\n--realm *\n--id=sip:$callee\@k.siptest.dev\n--username=$callee\n--password=t3st1ng\n--use-srtp=2\n--srtp-secure=0\n--stun-srv=10.20.7.60:3478\n--use-tls\n--null-audio\n--play-file=/root/wav/piano2.wav\n--auto-play\n--use-ice\n--use-turn\n--turn-srv=10.20.7.60:3478\n--turn-user=$callee\n--turn-passwd=$callee\n--auto-answer=200\n--duration=1200\n",$count,$count;
        close USERFILE;
        print STARTFILE "screen -S $callee -d -m pjsua --config-file /root/pjsua/conf/$callee.conf\n";
        print STOPFILE "screen -S $callee -p 0 -X stuff \"q\$(printf \\\\r)\"\n";
        $count++;
        $count++;
}
close STARTFILE;
close STOPFILE;
exit();
