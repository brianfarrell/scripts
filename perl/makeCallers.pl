#!/usr/bin/perl
my $network = "10.123.123.";
my $startUser = 1;
my $endUser = 401;
my $count = $startUser;
open (STARTFILE, ">/root/pjsua/scripts/startCallers.sh");
open (STOPFILE, ">/root/pjsua/scripts/stopCallers.sh");
while ($count < $endUser) {
        my $caller = sprintf("test%05d",$count);
        my $callee = sprintf("test%05d",$count+1);
        open (USERCONFFILE,">/root/pjsua/conf/$caller.conf");
        printf USERCONFFILE "--max-calls=1\n--rtp-port=4%04d\n--local-port=5%04d\n--log-file=/root/pjsua/log/pjsua$caller.log\n--log-level=5\n--app-log-level=4\n--color\n--registrar=sip:k.siptest.dev\n--realm *\n--id=sip:$caller\@k.siptest.dev\n--username=$caller\n--password=t3st1ng\n--use-srtp=2\n--srtp-secure=0\n--stun-srv=10.20.7.60:3478\n--use-tls\n--null-audio\n--play-file=/root/wav/piano2.wav\n--auto-loop\n--use-ice\n--use-turn\n--turn-srv=10.20.7.60:3478\n--turn-user=$caller\n--turn-passwd=$caller\n",$count,$count;
        close USERCONFFILE;
        print STARTFILE "screen -S $caller -d -m pjsua --config-file /root/pjsua/conf/$caller.conf sip:$callee\@k.siptest.dev\n";
        print STARTFILE "sleep 2\n";
        print STOPFILE "screen -S $caller -p 0 -X stuff \"q\$(printf \\\\r)\"\n";
        $count++;
        $count++;
}
close STARTFILE;
exit();
