# Usage: ./setpassword.sh <Database password>
#   This will generate the Drupal settings.php to be configured with that password

if [ $# -ne 2 ]; then
  echo "Usage: setpasswd.sh <db host> <db password>"
  exit 1;
fi

dbhost=$1
dbpassword=$2

echo Setting database host to: $1
echo Setting database password to: $2

WORKINGDIR="/var/www/sites/default"
FREERADDIR="/etc/freeradius"

cp "$WORKINGDIR/cp.settings.php.default" "$WORKINGDIR/settings.php"
sed -i "s/DATABASE_HOST/$dbhost/g" "$WORKINGDIR/settings.php"
sed -i "s/DATABASE_PASSWORD/$dbpassword/g" "$WORKINGDIR/settings.php"

cp "$FREERADDIR/default.roaming.pl" "$FREERADDIR/roaming.pl"
sed -i "s/DATABASE_HOST/$dbhost/g" "$FREERADDIR/roaming.pl"
sed -i "s/DATABASE_PASSWORD/$dbpassword/g" "$FREERADDIR/roaming.pl"

cp "$FREERADDIR/default.sql.conf" "$FREERADDIR/sql.conf"
sed -i "s/DATABASE_HOST/$dbhost/g" "$FREERADDIR/sql.conf"
sed -i "s/DATABASE_PASSWORD/$dbpassword/g" "$FREERADDIR/sql.conf"

