# ----------------------------------------------------------------------------
# Show command history from last Tuesday:
# ----------------------------------------------------------------------------
history | egrep "\| $(date -d'last Tuesday' +'%F')"
 
# ----------------------------------------------------------------------------
# Show command history from six days ago, 10-11 am:
# ----------------------------------------------------------------------------
history | egrep "\| $(date -d'-6 days' +'%F') (10|11):"
 
# ----------------------------------------------------------------------------
# Show command history for March 12, 7 am to March 16, 10 am:
# ----------------------------------------------------------------------------
range_hour() {
    TZ="$(date +"%Z")"; s="${1}"; e="${2}"; i=0
    se=$(date -d "${s}" +'%s'); ee=$(date -d "${e}" +'%s')
    sd="$(date -d "${s}")"; ed="$(date -d "${e}")"
    history | grep -E \
    "$(while [ ${se} -lt ${ee} ]; do
    st="$(date -d "${sd} + 1 hour" +'%F %H')"
    [[ ${i} < 1 ]] && echo -en "\| ${st}:*|" && (( i = i + 1 ))
    s="$(date -d "${sd} + 1 hour" +'%b %e %H')"
    sd="$(date -d "${s}")"; se=$(date -d "${s}" +'%s')
    echo -en "\| ${st}:*|"
    done | sed 's/|$//g')"
}
history | range_hour "Mar 12 07" "Mar 16 10"
 
# ----------------------------------------------------------------------------
# Show command history for March 18, 7:24 - 8:54 am:
# ----------------------------------------------------------------------------
range_minute() {
    TZ="$(date +"%Z")"; s="${1}"; e="${2}"; i=0
    se=$(date -d "${s}" +'%s'); ee=$(date -d "${e}" +'%s')
    sd="$(date -d "${s}")"; ed="$(date -d "${e}")"
    history | grep -E \
    "$(while [ ${se} -lt ${ee} ]; do
    st="$(date -d "${sd} + 1 minute" +'%F %H:%M')"
    [[ ${i} < 1 ]] && echo -en "\| ${st}:*|" && (( i = i + 1 ))
    s="$(date -d "${sd} + 1 minute" +'%b %e %H:%M')"
    sd="$(date -d "${s}")"; se=$(date -d "${s}" +'%s')
    echo -en "\| ${st}:*|"
    done | sed 's/|$//g')"
}
history | range_minute "Mar 18 07:24" "Mar 18 08:54"







# ----------------------------------------------------------------------------
# A function to create a compressed tarball of a directory in the current
# folder, if there's sufficient space
# ----------------------------------------------------------------------------
targz() {
  d="${1}"
  if [ -d "${d}" ]
  then
    if [ $(stat -f --printf="%a * %s / 1024\n" . | bc) -gt $(du -sk ./"${d}" | awk '{print $1}') ]
    then
      tar cvfz "${d}.tgz" "${d}"
    else
      echo "Low space in $(pwd)"
    fi
  else
    echo "Can't access ${d}"
  fi
}
 
# Running this will then produce folder.tgz in your home directory
cd ~ && targz folder





# ----------------------------------------------------------------------------
# The following function would rsync stuff from the specified source to the
# target with all the common and useful options.
# ----------------------------------------------------------------------------
myrsync() {
  s="${1}"
  t="${2}"
  if [ -d "${s}" ]
  then
    if [ ! -d "${t}" ]
    then
      echo "Creating target ${t}"
      mkdir -p "${t}"
    fi
    echo "Checking the size of ${s}"
    if [ $(du -sk "${s}" | awk '{print $1}') -lt \
    $(df -k "${t}" | sed 's/ \+/ /g' | grep -oP "(?<= )[0-9]{1,}(?= [0-9]{2}%)") ]
    then
      rsync -avuKxh --progress --log-file="$(mktemp)" "${s}" "${t}"
    else
      echo "Low space in ${t}"
    fi
  else
    echo "Can't access ${s}"
  fi
}
 
# An example of how to run the `myrsync` function:
myrsync /etc /opt/backups/$(date +'%Y-%m-%d_%H%M%S')
