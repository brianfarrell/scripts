#!/usr/bin/env sh

VERSION=1.0
script_name=`basename $0 | cut -d . -f1`
default_group='devshare'

usage() {
    cat << EOF

Usage: `basename $0` [ -g share_group ] path...

${script_name} creates one or more shared directories for the dev team.
The new directory path(s) may be expressed using absolute or relative notation.
By default the ${default_group} group will be the group owner of the shared directories.
You may specify an alternate group using the -g option.

This script requires elevated privileges, so it should be run with sudo.

v${VERSION}

EOF
    exit
}

if [ $# = 0 ]
then
    usage
fi

while [ $# -gt 0 ]      # Loop until no options left
do
    case $1 in          
    -g|--group) share_group=$2
        shift
        ;;
    -h|--help) usage
        ;;
    --) shift           # By convention, –– ends options
        break
        ;;
    -*) echo $0: $1: unrecognized option >&2
        exit
        ;;
    *)  break
        ;; 
    esac
    shift
done

if [ ${EUID} -ne 0 ]
then 
    echo "The ${script_name} script requires elevated privileges."
    echo "Please re-run with sudo."
    exit
fi

script_user=`logname`
share_group=${share_group:-${default_group}}

if [ `getent group ${share_group} | grep -c "^${share_group}"` -eq 0 ]
then 
    echo -n "The ${share_group} group does not exist. Create it now (y/n)? "
    read group_answer

    if echo "${group_answer}" | grep -iq "^y"
    then
        echo "Creating new share group: ${share_group}"
        groupadd ${share_group}
        echo "Adding ${script_user} to the ${share_group} group. You will need to add other users to this group as appropriate."
        usermod -a -G ${share_group} ${script_user}
    else
        echo "The ${share_group} group and requested directories will not be created."
        echo "Exiting..."
        exit
    fi
fi

while [ $# -gt 0 ]      # Loop until no args left
do
    request=$1
    new_share=`readlink -f ${request}`
    shift

    if [ -z ${new_share} ]
    then
        echo "The path ${request} is unresolved."
        echo "It may contain non-existant parent directories. These can be created as well."
        echo -n "Continue (y/n)? "
        read path_answer
        if echo "${path_answer}" | grep -iq "^y"
        then
            new_share=${request}
        else
            echo "The ${request} directory will not be created."
            continue
        fi
    fi

    if [ -e ${new_share} ]
    then
        echo "${new_share} already exists."
        continue
    fi

    echo "Creating new shared directory at ${new_share}"
    mkdir -p -m 775 ${new_share}
    # Here, we assume that we want the person running the script to be the owner of the newly created
    # directory. We could however have previously created a generic system user that would take on
    # the role of owner, if so desired...
    owner_user=${script_user}
    owner_group=${share_group}
    echo "Changing ownership of ${new_share} to ${owner_user}:${owner_group}"
    chown -R ${owner_user}:${owner_group} ${new_share}
    echo "Setting sgid bit on ${new_share}"
    chmod -R g+s ${new_share}
done
