#!/usr/bin/env bash

VERSION=1.0

usage() {
    cat << EOF

Usage: `basename $0` [options]

`basename $0` prepares an installation of the portal, radius, and mac addresses databases

v${VERSION}

Options:
    -d, --deploy <deployment>
        Run deployment-specific scripts.

    --deployment-dir <path>
        Path to alternate deployment directory. If --deployment-dir is specified, then --deploy is also set.
        Default: deployment/

    -e, --env <environment>
        The environment to which you are deploying.  Accepted Values: local | dev | qa | prod
        The environment *must* be specified or the script will exit without doing anything.

    -f, --force
        Force the import - create a new database, if necessary.  During the loading process, the name of the database
        is read from the header information inside the dump file.  If the database named already exists, that database
        is overwritten with the data from the dump file.  If database named *does not* already exist, then the data from
        the dump file is not loaded unless the <force> option is set.

    -h, --help
        Display this help and exit.

    --host
        Set the MySQL host to be used for this deployment.
        Default: localhost
    
    -i, --import <path>
        Import the database dumpfile(s) located at path.

    -n, --new
        New install - initialize a new set of databases for the Captive Portal.

    -p --password <password>
        Password to use for the MySQL login.
        Default: WaL1@y5

    --patch
        Apply patches.

    --patch-dir <path>
        Path to alterante patch directory.
        Default: sql/patch

    --patch-level <level>
        Apply patches up to and including the level speciifed
        Default: 99

    -t, --test
        Load test data for Users and Scratch Cards

    -u, --user <user>
        Username to use for the MySQL login.
        Default: root

EOF
    exit
}

if [[ $# = 0 ]]; then
    usage
fi

PLATFORM=`uname`
gnu_prefix=''

if [[ $PLATFORM = 'Darwin' ]]; then
    if [[ -z $(which pv) ]]; then
        echo "This script requires Pipe Viewer --> http://www.ivarch.com/programs/pv.shtml"
        echo "Please install with 'brew install pv' and try again."
        exit
    fi
    gnu_prefix='g'
fi

if [[ $PLATFORM = 'Linux' ]]; then
    if [ `dpkg-query -W -f='${Status}' pv 2>/dev/null | grep -c "ok installed"` -eq 0 ]; then
        echo "This script requires Pipe Viewer --> http://www.ivarch.com/programs/pv.shtml"
        echo "Please install with 'apt-get install pv' and try again."
        exit
    fi
fi

# set flag vars to defaults
deploy=
deployment_dir=
environment=
force=0
host=
data_dump=
new_install=0
pass=  
patch=0
patch_dir=
patch_level_cap=99
test_data=0
user= 

while [ $# -gt 0 ]      # Loop until no args left
do
    case $1 in          
    -d|--deploy) deploy=$2
        shift
        ;;
    --deployment-dir) deployment_dir=`${gnu_prefix}readlink -f ${2}`
        shift
        ;;
    -e|--env) environment=$2
        shift
        ;;
    -f|--force) force=1
        ;;
    -h|--help) usage
        ;;
    --host) host=${2}
        shift
        ;;
    -i|--import) data_dump=`${gnu_prefix}readlink -f ${2}`
        shift
        ;;
    -n|--new) new_install=1
        ;;
    -p|--password) pass=$2
        shift
        ;;
    --patch-dir) patch_dir=`${gnu_prefix}readlink -f ${2}`
        shift
        ;;
    --patch-level) patch_level_cap=$2
        shift
        ;;
    --patch) patch=1
        ;;
    -t|--test) test_data=1
        ;;
    -u|--user) user=$2
        shift
        ;;
    --) shift           # By convention, –– ends options
        break
        ;;
    -*) echo $0: $1: unrecognized option >&2
        ;;
    *)  break
        ;; 
    esac

    shift
done

if [[ -z ${environment} ]]
then
    printf "\n"
    echo "*** ERROR: Please specify the environment to which you are deploying. ***"
    printf "\n"
    usage
    exit
fi

pushd `dirname $0` > /dev/null
THIS_DIR=`pwd`
popd > /dev/null
INSTALL_HOME=${THIS_DIR%*/*}

# Set default values
default_user=root
default_pass=WaL1@y5
default_host=localhost

default_portal_db='portal'
default_radius_db='radius'
default_mac_addressses_db='mac_addresses'

if [[ -z ${deployment_dir} ]] && [[ ${environment} != 'local' ]]
then
    default_deployment_dir=${INSTALL_HOME}/deployment/sql
else
    default_deployment_dir=${INSTALL_HOME}/deployment/${deploy}/sql
fi

default_patch_dir=${INSTALL_HOME}/deployment/all/patch
default_test_data_dir=${INSTALL_HOME}/sql/reference_data/optional

# Set runtime values
user=${user:-${default_user}}
pass=${pass:-${default_pass}}
host=${host:-${default_host}}

portal_db=${default_portal_db}
radius_db=${default_radius_db}
mac_addresses_db=${default_mac_addressses_db}
db_set=(${portal_db} ${radius_db} ${mac_addresses_db})

deployment_dir=${deployment_dir:-${default_deployment_dir}}
patch_dir=${patch_dir:-${default_patch_dir}}
test_data_dir=${test_data_dir:-${default_test_data_dir}}

MYSQL_CMD=${MYSQL_CMD:-`which mysql`}
base_mysql_cmd="${MYSQL_CMD} --user=${user} --password=${pass} --host=${host}"

echo "Base directory: ${INSTALL_HOME}"
echo "Full Path to Dump Directory: ${data_dump}"
echo "MySQL user: ${user}"
echo "MySQL pass: ${pass}"
echo "Force: ${force}"
echo "New Install: ${new_install}"
echo "Apply Patches: ${patch}"
echo "Load Deployment Data: ${deploy}"
echo "Environment: ${environment}"
echo "Platform: ${PLATFORM}"

find_database() {
    the_database=`${base_mysql_cmd} --silent --skip-column-names --execute="SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='${1}'"`

    echo "$the_database"
}

initialize_new_install() {
    for db in ${db_set[*]}; do
        ${base_mysql_cmd} --silent --execute="DROP DATABASE IF EXISTS ${db};"
        echo "Creating Database: ${db}"
        ${base_mysql_cmd} --silent --execute="CREATE DATABASE ${db} CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
    done
    ${base_mysql_cmd} ${portal_db} < ${INSTALL_HOME}/sql/schema/portal_schema.sql
    ${base_mysql_cmd} ${mac_addresses_db} < ${INSTALL_HOME}/sql/schema/macaddress_schema.sql
    ${base_mysql_cmd} ${radius_db} < ${INSTALL_HOME}/sql/schema/radius_schema.sql

    ${base_mysql_cmd} ${portal_db} < ${INSTALL_HOME}/sql/reference_data/portal_init.sql
    ${base_mysql_cmd} ${portal_db} < ${INSTALL_HOME}/sql/reference_data/anon_and_admin_users.sql
    ${base_mysql_cmd} ${mac_addresses_db} < ${INSTALL_HOME}/sql/reference_data/macaddress_init.sql
    ${base_mysql_cmd} ${radius_db} < ${INSTALL_HOME}/sql/reference_data/radius_init.sql
}

load_database() {
    dumpfile=`basename ${1}`
    database=`head ${1} | egrep '.*Database:[[:space:]]+' | sed -E -e 's/.*Database:[[:space:]]+([[:graph:]])/\1/'`
    dump_date=`tail ${1} | egrep '.*Dump[[:space:]]+completed[[:space:]]+on' | sed -E -e 's/.*([[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}[[:space:]][[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2})/\1/'`
    database_exists=$(find_database ${database} "$@")
    
    echo "Loading ${database} from ${dumpfile} with Dump Date:" $dump_date
    ${base_mysql_cmd} --silent --execute="DROP DATABASE IF EXISTS ${database};"
    ${base_mysql_cmd} --silent --execute="CREATE DATABASE ${database} CHARACTER SET utf8 COLLATE utf8_unicode_ci;"
    cat ${1} | pv --progress --size `stat --printf="%s" ${1}` --name '  Loading' | ${base_mysql_cmd} ${database}
    ${base_mysql_cmd} --silent --execute="CREATE DEFINER=root@localhost FUNCTION ${database}.DUMP_DATE() RETURNS varchar(19) CHARSET utf8 RETURN '${dump_date}';"
}

apply_patches() {
    patch_table_exists=$(${base_mysql_cmd} ${portal_db} --execute="SELECT * FROM information_schema.tables WHERE table_schema = '${portal_db}' AND table_name = 'patch_level'")
    if [[ ${patch_table_exists} ]]
    then
        patch_level=$(${base_mysql_cmd} ${portal_db} -N --execute="SELECT MAX(patch_level) FROM patch_level")
    else
        patch_level=0
    fi
    
    echo "Looking in [${patch_dir}] for patches"
    if [ -e ${patch_dir} ]
    then
        for f in ${patch_dir}/*.sql
        do
            if [[ -f ${f} ]]
            then
                fBasename=$(basename ${f})
                arr=(${fBasename//./ })
                fileLevel=${arr[0]}
                echo "FILE LEVEL: ${fileLevel}"
                echo "PATCH LEVEL: ${patch_level}"
                if [ "${fileLevel}" -gt "${patch_level}" ] && [ "${fileLevel}" -le "${patch_level_cap}" ]
                then
                    echo "Applying patch ${fBasename}"
                    ${base_mysql_cmd} ${portal_db} < ${f}
                fi
            fi
        done
    fi
}

run_sql(){
    for f in $(find ${1} -iname '*.sql'); do
      filename=${f##*/}
      if [[ $filename = portal_*.sql ]]; then
        echo "  Loading file into portal db: $filename"
        ${base_mysql_cmd} portal < $f
      elif [[ $filename = radius_*.sql ]]; then
        echo "  Loading file into radius db: $filename"
        ${base_mysql_cmd} radius < $f
      else
        echo "  Skipping file: $filename"
      fi
    done
}

customize_deployment() {
    if [[ ${environment}  = "local" ]]
    then
      echo "Looking in deployment/all for scripts"
      run_sql  ${INSTALL_HOME}/deployment/all/
    fi

    if [[ ${environment}  = "dev" ]] || [[ ${environment}  = "qa" ]]
    then
      echo "Looking in deployment/all for scripts"
      run_sql  ${INSTALL_HOME}/deployment/all/

      echo "Loading ${environment}-specific data."
      run_sql  ${INSTALL_HOME}/deployment/${environment}/sql
    fi

    echo "Rebuilding Drupal's themes registry"
    echo "Looking in [${deployment_dir}] for deployment-specific stuff"
    run_sql ${deployment_dir}
}

load_test_data() {
    echo "Looking in [${test_data_dir}] for test data"
    for d in $(find ${test_data_dir} -type d)
    do
        if [[ ${d} != ${test_data_dir} ]]; then
            for f in $(find ${d} -iname *.sql)
            do
                echo "Loading Test Data: $(basename ${f})"
                ${base_mysql_cmd} $(eval echo \$$(basename ${d})_db) < ${f}
            done
        fi
    done
}

if [[ ${new_install} -eq 1 ]]; then
    initialize_new_install
fi

if [[ ${data_dump} ]] && [[ -d ${data_dump} ]]
then
    for f in ${data_dump}/*.sql
        do
            load_database ${f}
        done
elif [[ ${data_dump} ]] && [[ -f ${data_dump} ]]
then
    load_database ${data_dump}
fi

if [[ ${patch} -eq 1 ]]
then
    apply_patches ${patch_level_cap}
fi

if [[ ${deploy} ]] && [[ -d ${deployment_dir} ]] 
then
    customize_deployment
fi

if [[ ${test_data} -eq 1 ]]; then
    load_test_data
fi

echo 'The SC AES Patch must be run manually, if needed.'
echo 'Job Completed'
