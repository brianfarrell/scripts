#!/usr/bin/env bash

VERSION=1.0
script_name=`basename $0 | cut -d . -f1`
default_group='wizards'

usage() {
    cat << EOF

Usage: `basename $0` [ -g share_group ] path...

${script_name} creates a safe environment for untrusted SSH users.
The new directory path(s) may be expressed using absolute or relative notation.
By default the ${default_group} group will be the group owner of the shared directories.
You may specify an alternate group using the -g option.

This script requires elevated privileges.

v${VERSION}

EOF
    exit
}

if [ $# = 0 ]
then
    usage
fi

while [ $# -gt 0 ]      # Loop until no options left
do
    case $1 in          
    -g|--group) share_group=$2
        shift
        ;;
    -h|--help) usage
        ;;
    --) shift           # By convention, –– ends options
        break
        ;;
    -*) echo $0: $1: unrecognized option >&2
        exit
        ;;
    *)  break
        ;; 
    esac
    shift
done

if [ ${EUID} -ne 0 ]
then 
    echo "The ${script_name} script requires elevated privileges."
    echo "Please re-run with sudo."
    exit
fi

script_user=`logname`
share_group=${share_group:-${default_group}}

if [ `getent group ${share_group} | grep -c "^${share_group}"` -eq 0 ]
then 
    echo -n "The ${share_group} group does not exist. Create it now (y/n)? "
    read group_answer

    if echo "${group_answer}" | grep -iq "^y"
    then
        echo "Creating new share group: ${share_group}"
        groupadd ${share_group}
        echo "Adding ${script_user} to the ${share_group} group. You will need to add other users to this group as appropriate."
        usermod -a -G ${share_group} ${script_user}
    else
        echo "The ${share_group} group and requested directories will not be created."
        echo "Exiting..."
        exit
    fi
fi

while [ $# -gt 0 ]      # Loop until no args left
do
    request=$1
    new_share=`readlink -f ${request}`
    shift

    if [ -z ${new_share} ]
    then
        echo "The path ${request} is unresolved."
        echo "It may contain non-existant parent directories. These can be created as well."
        echo -n "Continue (y/n)? "
        read path_answer
        if echo "${path_answer}" | grep -iq "^y"
        then
            new_share=${request}
        else
            echo "The ${request} directory will not be created."
            continue
        fi
    fi

    if [ -e ${new_share} ]
    then
        echo "${new_share} already exists."
        continue
    fi

    echo "Creating new shared directory at ${new_share}"
    mkdir -p -m 775 ${new_share}
    # Here, we assume that we want the person running the script to be the owner of the newly created
    # directory. We could however have previously created a generic system user that would take on
    # the role of owner, if so desired...
    owner_user=${script_user}
    owner_group=${share_group}
    echo "Changing ownership of ${new_share} to ${owner_user}:${owner_group}"
    chown -R ${owner_user}:${owner_group} ${new_share}
    echo "Setting sgid bit on ${new_share}"
    chmod -R g+s ${new_share}
done



# The above code was taken from mkshare.sh and modified for this script.
# The above code is NOT modified completely yet for this purpose!!!!!!!!!!!!!!

# The code below this line is what was added as necessary for this script to accomplish it's goal.

########## Create the chroot jail ##########

D=/home/hogwarts
mkdir -p $D

mkdir -p $D/dev/
mknod -m 666 $D/dev/null c 1 3
mknod -m 666 $D/dev/tty c 5 0
mknod -m 666 $D/dev/zero c 1 5
mknod -m 666 $D/dev/random c 1 8

########## Set permissions ##########

chown root:root $D
chmod 0755 $D

# ls -ld $D  # to verify

########## Install bash shell in $D ##########

mkdir -p $D/bin

cp -v /usr/bin/bash $D/bin

# Copy required shared libs to $D directory. The syntax is as follows to find out what bash needed:
# ldd /bin/bash
#
##### Output:
#    linux-vdso.so.1 (0x00007ffef1d4d000)
#    libtinfo.so.6 => /lib64/libtinfo.so.6 (0x00007efeabf76000)
#    libdl.so.2 => /lib64/libdl.so.2 (0x00007efeabf6f000)
#    libc.so.6 => /lib64/libc.so.6 (0x00007efeabda4000)
#    /lib64/ld-linux-x86-64.so.2 (0x00007efeac10a000)

# Use the output of the `ldd` command for the below.

# Copy highlighted files one-by-one as follows using the cp command:
mkdir -p $D/lib
mkdir -p $D/lib64

cp -v /lib64/libtinfo.so.6.2 $D/lib64/
cp -v /lib64/libdl-2.32.so $D/lib64/
cp -v /lib64/libc-2.32.so $D/lib64/
cp -v /lib64/ld-2.32.so $D/lib64/
cp -v /lib64/libnss_files-2.32.so $D/lib64/

ln -s $D/lib64/libtinfo.so.6.2 $D/lib64/libtinfo.so.6
ln -s $D/lib64/libdl-2.32.so $D/lib64/libdl.so.2
ln -s $D/lib64/libc-2.32.so $D/lib64/libc.so.6
ln -s $D/lib64/ld-2.32.so $D/lib64/ld-linux-x86-64.so.2
ln -s $D/lib64/libnss_files-2.32.so $D/lib64/libnss_files.so.2

mkdir -p $D/etc
mkdir -p $D/home/{nathan,elizabeth,zachary}

adduser elizabeth
adduser nathan
adduser zachary

cp -vf /etc/{passwd,group} $D/etc/

chown -R elizabeth:elizabeth $D/home/elizabeth/
chown -R nathan:nathan $D/home/nathan/
chown -R zachary:zachary $D/home/zachary/
chmod -R 0700 $D/home/elizabeth
chmod -R 0700 $D/home/nathan
chmod -R 0700 $D/home/zachary

mkdir /home/{nathan,elizabeth,zachary}/.ssh

ssh-keygen -o -a 100 -t ed25519 -f /home/elizabeth/.ssh/id_ed25519 -C "elizabeth@trove.fm"
ssh-keygen -o -a 100 -t ed25519 -f /home/nathan/.ssh/id_ed25519 -C "nathan@trove.fm"
ssh-keygen -o -a 100 -t ed25519 -f /home/zachary/.ssh/id_ed25519 -C "zachary@trove.fm"

