#!/usr/bin/env bash

set -o nounset -o errexit

WORKDIR=$(dirname "$(realpath $0)")
CERTS=("isrgrootx1.pem" "isrg-root-x2.pem" "lets-encrypt-r3.pem" "lets-encrypt-e1.pem" "lets-encrypt-r4.pem" "lets-encrypt-e2.pem")

for CERT in "${CERTS[@]}"
do
  curl -o "$WORKDIR/$CERT" "https://letsencrypt.org/certs/$CERT"
done
