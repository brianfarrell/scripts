SELECT
`nasname`,
`nasipaddress`,
CAST(SUBSTRING_INDEX( `nasipaddress` , '.', 1 ) AS SIGNED) AS a,
CAST(SUBSTRING_INDEX(SUBSTRING_INDEX( `nasipaddress` , '.', 2 ),'.',-1) AS SIGNED) AS b,
CAST(SUBSTRING_INDEX(SUBSTRING_INDEX( `nasipaddress` , '.', -2 ),'.',1) AS SIGNED) AS c,
CAST(SUBSTRING_INDEX( `nasipaddress` , '.', -1 ) AS SIGNED) AS d
FROM nas
ORDER BY a,b,c,d ASC
