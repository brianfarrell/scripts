-- contacts sequence
CREATE SEQUENCE public.zip_lookup_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.zip_lookup_id_seq
    OWNER TO fluffy;

CREATE TABLE IF NOT EXISTS public.zip_lookup
(
    id bigint NOT NULL DEFAULT nextval('zip_lookup_id_seq'::regclass),
    country_code character varying(2) COLLATE pg_catalog."default",
    zip_code character varying(5) COLLATE pg_catalog."default",
    place_name character varying(180) COLLATE pg_catalog."default",
    state character varying(100) COLLATE pg_catalog."default",
    state_ab character varying(2) COLLATE pg_catalog."default",
    county character varying(50) COLLATE pg_catalog."default",
    locality_code character varying(8) COLLATE pg_catalog."default",
    lat double precision,
    "long" double precision,
    accuracy integer,
    CONSTRAINT zip_lookup_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.zip_lookup
    OWNER to fluffy;



-- contacts sequence
CREATE SEQUENCE public.contacts_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.contacts_id_seq
    OWNER TO fluffy;

-- contacts table
CREATE TABLE IF NOT EXISTS public.contacts
(
    id bigint NOT NULL UNIQUE DEFAULT nextval('contacts_id_seq'::regclass),
    app_role character varying(20) COLLATE pg_catalog."default",
    active boolean DEFAULT TRUE,
    CONSTRAINT contacts_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.contacts
    OWNER to fluffy;


-- companies table
CREATE TABLE IF NOT EXISTS public.companies
(
    contact_id bigint NOT NULL UNIQUE,
    name_company character varying(100) NOT NULL UNIQUE COLLATE pg_catalog."default",
    relationship character varying(20) NOT NULL COLLATE pg_catalog."default",
    CONSTRAINT companies_pkey PRIMARY KEY (contact_id),
    CONSTRAINT fk_contact
        FOREIGN KEY (contact_id)
            REFERENCES contacts(id)
            ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.companies
    OWNER to fluffy;


-- persons table
CREATE TABLE IF NOT EXISTS public.persons
(
    contact_id bigint NOT NULL UNIQUE,
    name_first character varying(30) COLLATE pg_catalog."default",
    name_last character varying(30) COLLATE pg_catalog."default",
    app_password character varying(100),
    salt character varying(100),
    company_id bigint,
    CONSTRAINT persons_pkey PRIMARY KEY (contact_id),
    CONSTRAINT fk_company
        FOREIGN KEY (company_id)
            REFERENCES companies(contact_id),
    CONSTRAINT fk_contact
        FOREIGN KEY (contact_id)
            REFERENCES contacts(id)
            ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.persons
    OWNER to fluffy;

-- addresses sequence
CREATE SEQUENCE public.addresses_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.addresses_id_seq
    OWNER TO fluffy;

-- addresses table
CREATE TABLE IF NOT EXISTS public.addresses
(
    id bigint NOT NULL UNIQUE DEFAULT nextval('addresses_id_seq'::regclass),
    contact_id bigint NOT NULL UNIQUE,
    addr1 character varying(100) NOT NULL COLLATE pg_catalog."default",
    addr2 character varying(100),
    city character varying(50),
    state character varying(2),
    zip_code character varying(5) COLLATE pg_catalog."default",
    CONSTRAINT addresses_pkey PRIMARY KEY (id),
    CONSTRAINT fk_contact
        FOREIGN KEY (contact_id)
            REFERENCES contacts(id)
            ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.addresses
    OWNER to fluffy;


-- email_addresses sequence
CREATE SEQUENCE public.email_addresses_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.email_addresses_id_seq
    OWNER TO fluffy;

-- email_addresses table
CREATE TABLE IF NOT EXISTS public.email_addresses
(
    id bigint NOT NULL UNIQUE DEFAULT nextval('email_addresses_id_seq'::regclass),
    contact_id bigint NOT NULL,
    email text NOT NULL COLLATE pg_catalog."default",
    email_label character varying(12),
    default_email boolean DEFAULT FALSE,
    login_email boolean DEFAULT FALSE,
    verified boolean DEFAULT FALSE,
    CONSTRAINT email_addresses_pkey PRIMARY KEY (id),
    CONSTRAINT fk_contact
        FOREIGN KEY (contact_id)
            REFERENCES contacts(id)
            ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE public.email_addresses
    OWNER to fluffy;


===========================================================================================================

WITH new_contact AS (
  INSERT INTO contacts(app_role)
  VALUES('none') RETURNING id
), new_company AS (
  INSERT INTO companies(contact_id, name_company, relationship)
  SELECT new_contact.id, 'VSS', 'other' FROM new_contact
  RETURNING contact_id
)
INSERT INTO addresses (contact_id, addr1, addr2, city, state, zip_code)
SELECT new_company.contact_id, '805 Channing Place, NE', NULL, 'Washington', 'DC', '20018' FROM new_company


WITH new_contact AS (
    INSERT INTO contacts(app_role)
    VALUES ('executive') RETURNING id
), new_person AS (
    INSERT INTO persons (contact_id, name_first, name_last, app_password, salt, company_id)
    SELECT new_contact.id, 'Stephen', 'Guest', 'password_hash_sg02', 'password_salt_sg02', 1 FROM new_contact
    RETURNING contact_id
)
INSERT INTO addresses (contact_id, addr1, addr2, city, state, zip_code)
SELECT new_person.contact_id, '2111 Wisconsin Avenue, NW', 'Apt. #620', 'Washington', 'DC', '20007' FROM new_person


INSERT INTO email_addresses (contact_id, email, email_label, default_email, login_email)
VALUES (2, 'sguest@vvswellness.com', 'work', TRUE, TRUE);

===========================================================================================================

WITH new_contact AS (
  INSERT INTO contacts(app_role)
  VALUES('none') RETURNING id
), new_company AS (
  INSERT INTO companies(contact_id, name_company, relationship)
  SELECT new_contact.id, 'Trove Supply', 'partner' FROM new_contact
  RETURNING contact_id
)
INSERT INTO addresses (contact_id, addr1, addr2, city, state, zip_code)
SELECT new_company.contact_id, '728 Live Oak Drive', NULL, 'McLean', 'VA', '22101' FROM new_company

WITH new_contact AS (
    INSERT INTO contacts(app_role)
    VALUES ('admin') RETURNING id
), new_person AS (
    INSERT INTO persons (contact_id, name_first, name_last, app_password, salt, company_id)
    SELECT new_contact.id, 'Brian', 'Farrell', 'password_hash_bf04', 'password_salt_bf04', 3 FROM new_contact
    RETURNING contact_id
)
INSERT INTO addresses (contact_id, addr1, addr2, city, state, zip_code)
SELECT new_person.contact_id, '6643 McLean Drive', NULL, 'McLean', 'VA', '22101' FROM new_person

INSERT INTO email_addresses (contact_id, email, email_label, default_email, login_email)
VALUES
    (4, 'brian.farrell@me.com', 'work', TRUE, TRUE),
    (4, 'brian.farrell@live.com', 'alt', FALSE, FALSE)

===========================================================================================================

WITH new_contact AS (
  INSERT INTO contacts(app_role)
  VALUES('none') RETURNING id
), new_company AS (
  INSERT INTO companies(contact_id, name_company, relationship)
  SELECT new_contact.id, 'Uncle Joe''s Farm', 'vendor' FROM new_contact
  RETURNING contact_id
)
INSERT INTO addresses (contact_id, addr1, addr2, city, state, zip_code)
SELECT new_company.contact_id, '1800 Sunset Boulevard', 'Suite 201', 'Los Angeles', 'CA', '90210' FROM new_company

===========================================================================================================

WITH new_contact AS (
  INSERT INTO contacts(app_role)
  VALUES('consumer') RETURNING id
), new_person AS (
    INSERT INTO persons (contact_id, name_first, name_last, app_password, salt)
    SELECT new_contact.id, 'Conrad', 'Brean', 'password_hash_cb06', 'password_salt_cb06' FROM new_contact
    RETURNING contact_id
)
INSERT INTO email_addresses (contact_id, email, email_label, default_email, login_email)
SELECT new_person.contact_id, 'conrad.brean@icloud.com', 'personal', TRUE, TRUE FROM new_person

===========================================================================================================

-- Get Street Address from Email Address
SELECT p.name_first, p.name_last, ea.email, a.addr1, a.addr2, a.city, a.state, a.zip_code
FROM contacts, persons AS p, addresses AS a, email_addresses AS ea
WHERE contacts.id = ea.contact_id
AND p.contact_id = ea.contact_id
AND a.contact_id = ea.contact_id
AND ea.email = 'brian.farrell@me.com'

-- Get all email addresses for user, based on one email address
SELECT * FROM email_addresses
LEFT JOIN contacts
ON email_addresses.contact_id = contacts.id
WHERE contacts.id =
    (SELECT contact_id
     FROM email_addresses
     WHERE email = 'brian.farrell@me.com'
    )
