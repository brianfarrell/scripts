#!/usr/bin/env python


"""
Script to ensure that filenames are lower-case, with underscores instead of spaces.
"""

import argparse
import logging
import os
from pathlib import Path
import re


BASE_DIR = Path(__file__).parent
LOG_FILE = str(BASE_DIR.joinpath('kosher.log'))

IGNORE = ['.DS_Store']

logging.basicConfig(
    format='%(asctime)s\t%(levelname)s\t%(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=logging.INFO,
    handlers=[
        logging.FileHandler(LOG_FILE, mode='a', encoding='utf-8'),
        logging.StreamHandler()
    ]
)

parser = argparse.ArgumentParser(
    prog='build',
    description='Build the React UI in a Docker container and upload to Cove.',
    conflict_handler='resolve',
    formatter_class=argparse.RawTextHelpFormatter,
    epilog='\n \n',
)

parser.add_argument(
    'path',
    action='store',
    default='.',
    nargs='?',
    help="Specify the path to normalize."
)


def make_kosher(target_path):
    file_list = os.scandir(target_path)

    for file in file_list:
        if file.is_dir():
            logging.info(f"Found directory: {file.path}")
            os.chdir(file.path)
            make_kosher(file.path)
        else:
            if file.name not in IGNORE:
                src_dir = Path(file.path).parent
                kosher_filename = re.sub(r'\s', "_", file.name)
                logging.info(f"Renaming {file.path}")
                new_path = Path(src_dir, kosher_filename.lower())
                logging.info(f"New Path: {new_path.resolve()}")
                os.replace(file.path, new_path)


if __name__ == '__main__':
    args = parser.parse_args()
    target_path = Path(args.path).resolve()

    make_kosher(target_path)
