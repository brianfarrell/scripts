#!/usr/bin/env python

import json
import sys
import re

try:
    dotenv = sys.argv[1]
except IndexError:
    dotenv = '.env'

with open(dotenv, 'r') as f:
    content = f.readlines()

content_list = [x.strip().split('#')[0].split('=', 1) for x in content if '=' in x.split('#')[0]]
content_dict = dict(content_list)

for k, v in content_list:
    for i, x in enumerate(v.split('$')[1:]):
        key = re.findall(r'\w+', x)[0]
        v = v.replace('$' + key, content_dict[key])
    content_dict[k] = v

print(json.dumps(content_dict))
