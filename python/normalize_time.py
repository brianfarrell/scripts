from datetime import datetime, timedelta

import pytz
from tzlocal import get_localzone


class SystemTime:
    @staticmethod
    def get_aware_utc_now() -> datetime:
        return datetime.now(pytz.utc).replace(microsecond=0)

    @staticmethod
    def get_aware_local_now() -> datetime:
        return datetime.now(get_localzone()).replace(microsecond=0)

    @staticmethod
    def device_timestamp_to_utc(timestamp: str) -> datetime:
        device_time = datetime.strptime(timestamp, '%Y%m%d%H%M%S')
        # using replace for tzinfo is ONLY safe for UTC!
        device_time = device_time.replace(
            microsecond=0).replace(tzinfo=pytz.utc)
        return device_time

    @staticmethod
    def device_mdy_timestamp_to_utc(timestamp: str) -> datetime:
        device_time = datetime.strptime(timestamp, '%m%d%Y%H%M%S')
        # using replace for txinfo is ONLY safe for UTC!
        device_time = device_time.replace(
            microsecond=0).replace(tzinfo=pytz.utc)
        return device_time

    @staticmethod
    def screen_scrape_time_to_utc(timestamp: str) -> datetime:
        screen_time = datetime.strptime(timestamp, '%m/%d/%Y %X')
        local_zone = get_localzone()
        # cannot safely use replace(txinfo=*)!!!
        localize_screen_time = local_zone.localize(screen_time, is_dst=True)
        normalized_screen_time = local_zone.normalize(localize_screen_time)
        utc_screen_time = normalized_screen_time.astimezone(pytz.utc)
        return utc_screen_time

    @staticmethod
    def utc_to_screen_scrape(error_prefix: str, utc_dt: datetime) -> str:
        SystemTime._assert_tz(error_prefix + ' utc_dt', utc_dt)
        screen_time = utc_dt.astimezone(get_localzone())
        return screen_time.strftime('%m/%d/%Y %X')

    @staticmethod
    def assert_harness_time_equality(
            error_prefix: str,
            date0: datetime,
            date1: datetime) -> None:
        SystemTime._assert_tz(f'{error_prefix} '
                              f'assert_harness_time_equality: date0', date0)
        SystemTime._assert_tz(f'{error_prefix} '
                              f'assert_harness_time_equality: date1', date1)
        assert date0 == date1, (f'{error_prefix}: not equal:\n\t'
                                f'{date0.isoformat()}\n\t{date1.isoformat()}')

    @staticmethod
    def assert_time_within_range(
            error_prefix: str,
            start: datetime,
            check: datetime,
            stop: datetime,
            delta_minutes=2
    ) -> None:
        """Check to see if a time is between (inclusive) a
        start and stop time 'delta_minutes' minutes

        Delta will mostly accommodate System latency time for the bound 'stop'.

        Args:
            error_prefix (string):
            start (datetime): The start time (lower bound).
            check (datetime): The time to check.
            stop (datetime): The end time (upper bound).
            delta_minutes (int): time to expand acceptable range before
                                 start and after stop.

        """
        SystemTime._assert_tz(f'{error_prefix} assert_time_within_range: '
                              f'start', start)
        SystemTime._assert_tz(f'{error_prefix} assert_time_within_range: '
                              f'check', check)
        SystemTime._assert_tz(f'{error_prefix} assert_time_within_range: '
                              f'stop', stop)

        if delta_minutes != 0:
            delta = timedelta(minutes=delta_minutes)
            start -= delta
            stop += delta
        assert start <= check, (f'{error_prefix} check:\n\t{check} '
                                f'is ahead of start:\n\t{start}')
        assert check <= stop, (f'{error_prefix} check:\n\t{check} '
                               f'is later than stop:\n\t{stop}')

    @staticmethod
    def _assert_tz(prefix: str, date: datetime) -> None:
        error_msg = ('all harness time math and asserting is expected to be '
                     'UTC TZ \'aware\'.\n'
                     'Please leverage the MTTime module functions to convert '
                     'this time as expected.')

        assert isinstance(date, datetime), \
            (f'{date}: {datetime} is not the expected '
             f'\'datetime\' type;\n{error_msg}')

        assert date.tzinfo is pytz.utc, \
            (f'{prefix}: {date.tzinfo} is not UTC TZ;\n'
             f'{error_msg}')

    @staticmethod
    def formatted_time_to_seconds(time_format: str, time_str: str) -> float:
        time_obj = datetime.strptime(time_str, time_format)
        time_delta_obj = timedelta(hours=time_obj.hour,
                                   minutes=time_obj.minute,
                                   seconds=time_obj.second)
        seconds = time_delta_obj.total_seconds()
        return seconds
