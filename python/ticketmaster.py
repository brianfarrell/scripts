#!/usr/local/venv/ticketmaster/bin/python

import argparse
import json
import logging
import logging.config
import os
import sqlite3
import sys
from textwrap import dedent
import time

from jira.client import JIRA
from pyzabbix import ZabbixAPI

TM_HOME = os.path.dirname(os.path.abspath(__file__))
TICKETMASTER_CONFIG = os.path.join(TM_HOME, 'ticketmaster/tm_config.json')
LOGGING_CONFIG = os.path.join(TM_HOME, 'ticketmaster/tm_logging.json')
TMDB_INSTANCE = os.path.join(TM_HOME, 'ticketmaster/tmdb.sqlite')


class Ticketmaster:
    """ticketmaster manages Jira tickets for Zabbix events"""

    def __init__(self, source=None, jira_instance=None, event=None):
        self.config = self.load_config()
        self.tmdb = TMDBClient(TMDB_INSTANCE)

        if event:
            self.event = json.loads(event)
            self.event['source'] = source
            self.event['jira_instance'] = jira_instance

    def tmdb(self):
        self.tmdb

    def load_config(self, default_path=TICKETMASTER_CONFIG, env_key='TM_CFG'):
        """Load the configuration file for ticketmaster.

        Args:
          default_path: path to the config file
          env_key: optional environment variable set to
                   indicate path to config file

        Returns:
          Dictionary of configuration details
        """
        path = default_path
        value = os.getenv(env_key, None)
        if value:
            path = value

        try:
            with open(path, 'rt') as f:
                config = json.load(f)
            logger.debug('Ticketmaster configuration loaded')
            return config
        except Exception as config_error:
            logger.exception(config_error)

    def queue_event(self):
        """Insert new messages into the queue"""
        self.tmdb.queue_event(self.event)
        if self.event['source'] == 'zabbix':
            self.ack_zabbix_event('queued', self.event)

    def process_queue(self):
        event = True
        while event:
            event = self.tmdb.get_queue(limit=1)
            if event:
                self.process_event(event)

    def process_event(self, event):
        """Determine what action(s) to take, based on the contents of
        the message sent from Zabbix and the state of any
        related issue in Jira.
        """
        if 'PROBLEM' in event['trigger_status']:
            issues = self.search_for_issues(event)
            if issues.total == 0:
                logger.debug('This is a PROBLEM with 0 Open Issues')
                jira_ticket = self.create_jira_ticket(event)
                event['jira_ticket'] = jira_ticket.key
                event['jira_ticket_url'] = \
                    (f"{self.config['jira'][event['jira_instance']]['url']}"
                     f"browse/{event['jira_ticket']}")
                self.ack_zabbix_event('processed', event)
            else:
                jira = self.login_to_jira(event['jira_instance'])
                for x in issues:
                    logger.debug(
                        f"This is a PROBLEM with {issues.total} Open Issues"
                    )
                    new_comment = (f"Received new notification of this event "
                                   f"on {event['event_date']} at "
                                   f"{event['event_time']}.")
                    jira.add_comment(x.key, new_comment)
                    logger.info(f"Added new comment to {str(x.key)}")
                    event['jira_ticket'] = x.key
            self.tmdb.update_event_record(event)

        if 'OK' in event['trigger_status']:
            issues = self.search_for_issues(event)
            jira = self.login_to_jira(event['jira_instance'])
            for x in issues:
                event['jira_ticket'] = str(x.key)
                logger.debug(f"Time to RESOLVE Issue {event['jira_ticket']}")
                assignee = x.fields.assignee

                if assignee is None:
                    logger.debug("Issue is not currently assigned to anyone "
                                 "- closing ticket")
                    self.close_jira_ticket(event, x)
                    self.ack_zabbix_event('closed', event)
                else:
                    logger.debug(f"Issue is currently assigned to: "
                                 f"{assignee.displayName}")
                    new_comment = ("Received recovery message from Zabbix. "
                                   "Trigger status for this event is now 'OK'")
                    jira.add_comment(x.key, new_comment)
                    logger.info(f"Added new comment to {str(x.key)}")
                    self.ack_zabbix_event('comment_assigned', event)
                self.tmdb.update_event_record(event)

    def login_to_jira(self, jira_instance):
        """Initiate Jira session."""
        jira_user = self.config['jira'][jira_instance]['username']
        jira_pass = self.config['jira'][jira_instance]['password']
        jira_url = self.config['jira'][jira_instance]['url']
        check_jira_cert = self.config['jira'][jira_instance]['verify']

        try:
            jira = JIRA(options={'server': jira_url,
                                 'verify': check_jira_cert},
                        basic_auth=(jira_user, jira_pass))
            logger.debug('Successful login to Jira.')
            return jira
        except Exception as conn_error:
            logger.exception('Jira login failed')
            sys.exit()

    def search_for_issues(self, event):
        """Search for issues in Jira that correspond with the
        Event ID in the Zabbix message
        """
        jira = self.login_to_jira(event['jira_instance'])
        jira_event_id = f"{event['source']}_{event['event_id']}"
        issues = jira.search_issues(
            'project = {0} AND \"Event ID\" ~ \"\\"{1}\\"\"'.format(
                event['jira_project'], jira_event_id)
        )
        logger.debug(f"Total issues found for {event['event_id']}: "
                     f"{str(issues.total)}")
        return issues

    def create_jira_ticket(self, event):
        """Create issue in Jira for the Event ID given in the Zabbix message"""
        e = event
        jira = self.login_to_jira(e['jira_instance'])
        event_url = (f"{self.config['zabbix']['url']}tr_events.php?triggerid="
                     f"{str(e['trigger_id'])}&eventid={str(e['event_id'])}")

        summary = f"**ALARM** {e['host']}: {e['trigger']}"

        description = f"""
            Host: {e['host']}

            Trigger: {e['trigger']}
            Trigger Severity: {e['trigger_severity']}

            Event ID: {e['event_id']}
            Event Date: {e['event_date']}
            Event Time: {e['event_time']}

            {e['item_name']}: {e['item_value']}

            Event URL: {event_url}
            """

        priorities = self.config['jira'][
            e['jira_instance']][e['jira_project']]['priorities']

        issue_fields = self.config['jira'][
            e['jira_instance']][e['jira_project']]['issue_fields']

        jira_event_id = e['source'] + "_" + e['event_id']

        meta = jira.createmeta(
            projectKeys=e['jira_project'],
            issuetypeNames=issue_fields['issuetype']['name'],
            expand='projects.issuetypes.fields'
        )

        for p in meta['projects']:
            for i in p['issuetypes']:
                for f in i['fields'].keys():
                    if i['fields'][f]['name'] == 'Host':
                        host_field = f
                    if i['fields'][f]['name'] == 'Event ID':
                        new_event_id_field = f

        mutable_fields = {
            "summary": summary,
            "description": dedent(description),
            "priority": {"name": priorities[e['trigger_severity']]},
            new_event_id_field: jira_event_id,
            host_field: e['host']
        }

        issue_fields.update(mutable_fields)

        try:
            jira_ticket = jira.create_issue(fields=issue_fields)
            logger.info('Created Jira Ticket %s', jira_ticket.key)
            return jira_ticket
        except Exception as create_ticket_error:
            logger.exception('Failed to create Jira Ticket')

    def close_jira_ticket(self, event, issue):
        jira = self.login_to_jira(event['jira_instance'])
        transition_id = '-1'
        transitions = jira.transitions(issue)
        for t in transitions:
            if t['name'] == 'Close Issue':
                transition_id = t['id']
        if int(transition_id) > -1:
            logger.debug('Transition ID: %s', transition_id)
            jira.transition_issue(
                issue.key,
                transition_id,
                resolution={'id': '10100'}
            )
            logger.info(f"Set resolution for {event['jira_ticket']} to "
                        f"'zabbix_closed'")
        else:
            logger.info(f"Unable to resolve {event['jira_ticket']} as "
                        f"'zabbix_closed'. No further action taken.")

    def ack_zabbix_event(self, action, event=None):
        """Login to Zabbix and ACK event."""
        try:
            if action == 'queued':
                ack_message = 'Event queued for entry into Jira.'
            if action == 'processed':
                ack_message = (f"Jira Ticket created:"
                               f"{event['jira_ticket_url']}\n")
            if action == 'closed':
                ack_message = (f"Trigger Status now 'OK'. "
                               f"Closed Jira Ticket: {event['jira_ticket']}")
            if action == 'comment_assigned':
                ack_message = (f"Trigger Status now 'OK', but Jira Ticket "
                               f"{event['jira_ticket']} has already been "
                               f"assigned."
                               f"\nTicket must be closed by assignee.")
            zh = ZabbixAPI('http://0.0.0.0/zabbix/')
            zh.login(
                user=self.config['zabbix']['username'],
                password=self.config['zabbix']['password']
            )
            zh.event.acknowledge(
                eventids=event['event_id'],
                message=ack_message
            )
        except Exception as e:
            logger.exception('Failed to ACK Zabbix Event')
        else:
            logger.info('ACK sent to Zabbix Event %s', event['event_id'])


class TMDBClient():
    """TMDBClient provides a connection to the Ticketmaster Database (TMDB)
    and methods to interact with it.
    """

    def __init__(self, instance):
        self.db = sqlite3.connect(instance)
        self.db.row_factory = sqlite3.Row
        self.cursor = self.db.cursor()
        logger.info("Initialized connection to TMDB")

    def clear_queue(self):
        sql = "DELETE FROM alarm_events"
        self.cursor.execute(sql)
        self.db.commit()
        logger.info("Queue cleared in alarm_events")

    def close(self):
        self.db.close()
        logger.info("Closed connection to TMDB")

    def display_table(self, table):
        sql = "SELECT * FROM {0}".format(table)
        self.cursor.execute(sql)
        rows = self.cursor.fetchall()

        for row in rows:
            print(row)

        logger.info(f"Displayed {table} table")

    def queue_event(self, event):
        """Insert new messages into the queue"""
        e = event
        self.cursor.execute('''INSERT INTO alarm_events(source, host, trigger, trigger_id, trigger_status, trigger_severity,
                                      event_id, event_date, event_time, item_name, item_value, jira_instance, jira_project)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)''',
                                (e['source'], e['host'], e['trigger'], e['trigger_id'], e['trigger_status'], e['trigger_severity'],
                                 e['event_id'], e['event_date'], e['event_time'], e['item_name'], e['item_value'], 
                                 e['jira_instance'], e['jira_project']))
        self.db.commit()
        logger.info(f"Message queued for Event {e['event_id']}, "
                    f"Trigger Status: {e['trigger_status']}")

    def get_queue(self, **kwargs):
        args = {}
        if kwargs is not None:
            for key, value in kwargs.iteritems():
                args[key] = value

        sql = "SELECT * FROM alarm_events WHERE jira_ticket ISNULL"

        if args['limit']:
            sql = sql + " LIMIT {0}".format(args['limit'])

        self.cursor.execute(sql)
        queued_event = self.cursor.fetchone()
        if queued_event:
            event = dict(zip(queued_event.keys(), queued_event))
        else:
            event = None

        return event

    def update_event_record(self, event):
        e = event
        print(e)
        sql = (f"UPDATE alarm_events"
               f"SET jira_ticket = \"{e['jira_ticket']}\""
               f"WHERE source = \"{e['source']}\""
               f"AND event_id = \"{e['event_id']}\""
               f"AND trigger_status = \"{e['trigger_status']}\""
               f"AND jira_ticket ISNULL")
        self.cursor.execute(dedent(sql))
        self.db.commit()


class GMTFormatter(logging.Formatter):
    """
    The GMTFormatter class is availble, if the desired time format in the log
    file is UTC. To switch this 'On', simply add the following line to the
    formatter section of the config file:

    "()": "__main__.GMTFormatter"

    """
    converter = time.gmtime


def setup_logging(
    default_path=LOGGING_CONFIG,
    default_level=logging.DEBUG,
    env_key='LOG_CFG'
):
    """Setup logging configuration."""
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

    return logging.getLogger('ticketmaster')


## Main program for testing.
#
#
def main():
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '-c',
        '--clear',
        action='store_true',
        help="Clear the queue"
    )

    group.add_argument(
        '-d',
        '--display',
        type=str,
        help="Display the tmdb table given in the argument"
    )

    group.add_argument(
        '-p',
        action='store_true',
        help="Process events queued in tmdb"
    )

    group.add_argument(
        '-t',
        '--test',
        action='store_true',
        help="Run tests"
    )

    parser.add_argument(
        'source',
        help="Source of the alert message",
        nargs='?',
        default=None
    )

    parser.add_argument(
        'jira_instance',
        help="Name of Jira instance to use for tickets",
        nargs='?',
        default=None
    )

    parser.add_argument(
        'event',
        help="A Zabbix alert event",
        nargs='?',
        default=None
    )

    args = parser.parse_args()

    event = None

    if args.p:
        if args.source:
            error_message = "No other arguments are allowed with argument -p"
            logger.error(error_message)
            parser.error(error_message)
        logger.info('Processing event queue')
        tm = Ticketmaster()
        tm.process_queue()
        tm.tmdb.close()
    elif args.test:
        source = 'zabbix'
        jira_instance = 'jira'
        event = ('{"host": "r2d2.example.com",'
                 ' "trigger": "Holy Shnikes Trigger",'
                 ' "trigger_id": "TST34442",'
                 ' "trigger_status": "PROBLEM",'
                 ' "trigger_severity": "Average",'
                 ' "event_id": "100028",'
                 ' "event_date": "2015.02.26",'
                 ' "event_time": "20:42:33",'
                 ' "item_name": "Test Item",'
                 ' "item_value": "250", "jira_project": "TMTST"}')
    elif args.display:
        tmdb = TMDBClient(TMDB_INSTANCE)
        tmdb.display_table(args.display)
        tmdb.close()
    elif args.clear:
        tmdb = TMDBClient(TMDB_INSTANCE)
        tmdb.clear_queue()
        tmdb.close()
    elif args.event:
        error_message = ("If you are specifying a source, you must also "
                         "specify a jira_instance and an event.")
        if not args.source:
            logger.error(error_message)
            parser.error(error_message)
        if not args.jira_instance:
            logger.error(error_message)
            parser.error(error_message)
        source = args.source
        jira_instance = args.jira_instance
        event = args.event
    else:
        parser.print_help()

    if event:
        tm = Ticketmaster(source, jira_instance, event)
        tm.queue_event()
        tm.tmdb.close()


if __name__ == '__main__':
    logger = setup_logging()
    sys.exit(main())
