"""
harness.utils
~~~~~~~~~~~~~~~~

This module provides helper functions used during testing.

"""

from decimal import Decimal, ROUND_DOWN, localcontext
import logging
from math import floor
import os
import random
import re
import stat
import subprocess
import sys
from textwrap import dedent
import time
from typing import Dict
from urllib import parse as url_parse

import mysql.connector
import paramiko
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FFOptions
from sshtunnel import SSHTunnelForwarder
import yaml

from config import harness_settings
from normalize_time import SystemTime

test_run_start = SystemTime.get_aware_utc_now()
logger = logging.getLogger('harness.utils')

HARNESS_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = os.path.join(HARNESS_PATH, 'fixtures', 'data')
sys.path.append(HARNESS_PATH)

DEVICE_PROTOCOL_VERSION = '001'


class Esc(object):
    BOLD = '\033[1m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    END = '\033[0m'
    GREEN = '\033[92m'
    PURPLE = '\033[95m'
    RED = '\033[91m'
    UNDERLINE = '\033[4m'
    YELLOW = '\033[93m'


settings = None


def configure_harness(env, alt, config_file):
    global settings
    config_classes = {'ci': 'CIConfig', 'dev': 'BaseConfig', 'vm': 'VMConfig'}
    settings = harness_settings.Config()
    settings['ENV'] = env
    settings.from_object(f'harness.config.harness_settings:'
                         f'{config_classes[env]}')
    if alt:
        settings['HARNESS_CONFIG_CUSTOM_ALT'] = \
            os.getenv('HARNESS_CONFIG_CUSTOM_ALT')
        settings.from_envvar('HARNESS_CONFIG_CUSTOM_ALT', silent=True)
    else:
        settings['HARNESS_CONFIG_CUSTOM'] = os.getenv('HARNESS_CONFIG_CUSTOM')
        settings.from_envvar('HARNESS_CONFIG_CUSTOM', silent=True)
    if config_file:
        settings['HARNESS_CONFIG_CLI_OPTION'] = config_file
        settings.from_pyfile(config_file)


def show_settings(env, alt, config_file):
    global settings
    for k, v in iter(settings.items()):
        print(f'\n{k}: {v}')
    print('\n')


def get_ssh_session(host, username, password=None, key_filename=None):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(
            host,
            username=username,
            password=password,
            key_filename=key_filename
        )
        return ssh
    except paramiko.SSHException:
        logger.critical("SSH Connection Failed")
        raise


def refresh_system(deploy=False):
    if settings['MODE'] == 'vm':
        logger.info(f'Running System Install Script on '
                    f'{settings["HOST_SYSTEM_NAME"]}')
        commands = []
        if deploy:
            ssh = get_ssh_session(
                settings['HOST_SYSTEM_NAME'],
                settings['HOST_SYSTEM_SSH_USER'],
                password=settings['HOST_SYSTEM_SSH_PASS'],
                key_filename=settings['HOST_SYSTEM_KEYFILE'],
            )

            cmd = (f'sudo package/installUpdateSystem_StripedAlder_*.sh '
                   f'-quiet true -sysDB {settings["DB_SYSTEM_NAME"]} '
                   f'-overwriteDB true '
                   f'-additionalDBScriptDir {settings["DB_SQL_CUSTOM_DIR"]} '
                   f'-overwriteBackup true')
        else:
            ssh = get_ssh_session(
                settings['HOST_DB_NAME'],
                settings['HOST_DB_SSH_USER'],
                password=settings['HOST_DB_SSH_PASS'],
                key_filename=settings['HOST_DB_KEYFILE'],
            )
            # The host is localhost b/c we've already tunneled-in to
            # the actual host
            cmd = (f'{settings["HOST_DB_SCRIPT_DIR"]}/refreshSystemDB.sh '
                   f'-host localhost -port {settings["HOST_DB_PORT"]} '
                   f'-sysDB {settings["DB_SYSTEM_NAME"]} '
                   f'-rootUser {settings["DB_SYSTEM_USER"]} '
                   f'-rootPass {settings["DB_SYSTEM_PASS"]} '
                   f'-baseDir {settings["DB_SQL_BASE_DIR"]} '
                   f'-addScriptDir {settings["DB_SQL_CUSTOM_DIR"]}')

            remote_path = settings['HOST_DB_SCRIPT_DIR']
            refresh_script = os.path.join(
                HARNESS_PATH,
                'tools',
                'refreshSystemDB.sh'
            )
            local_base_sql = os.path.join(HARNESS_PATH, '..', 'sql')

            with paramiko.SFTPClient.from_transport(
                    ssh.get_transport()) as sftp:
                sftp.put(
                    refresh_script,
                    os.path.join(
                        remote_path,
                        os.path.basename(refresh_script)
                    )
                )
                sftp.chmod(
                    os.path.join(
                        remote_path,
                        os.path.basename(refresh_script)
                    ),
                    stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP |
                    stat.S_IROTH | stat.S_IXOTH)
                os.chdir(os.path.split(local_base_sql)[0])
                parent = os.path.split(local_base_sql)[1]
                for walker in os.walk(parent):
                    try:
                        sftp.mkdir(os.path.join(remote_path, walker[0]))
                    except Exception as e:
                        raise e
                    for file in walker[2]:
                        sftp.put(os.path.join(
                            walker[0], file),
                            os.path.join(remote_path, walker[0], file)
                        )
        commands.append(cmd)

        for cmd in commands:
            stdin, stdout, stderr = ssh.exec_command(cmd)
            time.sleep(2)

        for line in stdout.readlines():
            logger.info(f'(System Install) {line.strip()}')

        for line in stderr.readlines():
            logger.error(f'(System Install) {line.strip()}')

        ssh.close()

        logger.info("Waiting 1 minute for servers to startup")
        time.sleep(60)


def archive_log_files():
    timestamp = test_run_start.strftime('_%Y%m%d_%H%M%S')
    if settings['MODE'] == 'vm':
        logger.info(f'Archiving System Log on {settings["HOST_SYSTEM_NAME"]}')

        ssh = get_ssh_session(
            settings['HOST_SYSTEM_NAME'],
            settings['HOST_SYSTEM_SSH_USER'],
            password=settings['HOST_SYSTEM_SSH_PASS'],
            key_filename=settings['HOST_SYSTEM_KEYFILE']
        )

        cmds = [
            (
                f'cd /tomcat/System/ && '
                f'sudo tar -zcvf /home/sys/system_log_archive/test_run'
                f'{timestamp}.tar.gz logs'
            ),
            'sudo rm /tomcat/System/logs/*'
        ]

        for cmd in cmds:
            stdin, stdout, stderr = ssh.exec_command(cmd)
            for line in stdout.readlines():
                logger.info(line.strip())
            for line in stderr.readlines():
                logger.error(line.strip())


def get_driver(browser):
    mode = settings.get('MODE')
    if not browser:
        browser = settings.get('ACTIVE_DRIVER', 'firefox')
    # Currently, we only support Chrome and Firefox
    if browser.lower() == 'firefox':
        firefox_profile = settings.get('FIREFOX_PROFILE')
        if firefox_profile:
            profile = webdriver.FirefoxProfile(
                os.path.expanduser(firefox_profile)
            )
        else:
            profile = webdriver.FirefoxProfile()
        profile.accept_untrusted_certs = True
        options = FFOptions()
        options.add_argument('-foreground')
        # options.add_argument('-devtools')
        driver = webdriver.Firefox(
            firefox_profile=profile,
            firefox_options=options
        )
    elif browser.lower() == 'chrome':
        options = webdriver.ChromeOptions()
        if mode == 'ci':
            options.add_argument("--no-sandbox")
            logger.info("Adding Chrome option '--no-sandbox' "
                        "(we're running as root)")
        chrome_emulation = settings.get('CHROME_EMULATION')
        if chrome_emulation:
            logger.info("Adding Chrome option 'chrome_emulation'")
            options.add_experimental_option(
                "mobileEmulation", chrome_emulation
            )
        options.add_argument("--disable-session-crashed-bubble")
        options.add_argument("--disable-infobars")
        headless = settings.get('HEADLESS')
        if headless:
            logger.info("Running Chrome Browser in Headless Mode")
            options.add_argument('--headless')
            logger.info("Adding Chrome option '--headless'")
            options.add_argument('--window-size=1360,768')
            logger.info("Adding Chrome option '--window-size=1360,768'")
        options.add_argument("--ignore-certificate-errors")
        chrome_profile_root = settings.get('CHROME_PROFILE_ROOT')
        if chrome_profile_root:
            profile_root = os.path.expanduser(chrome_profile_root)
            logger.info(f"Adding Chrome option "
                        f"'--user-data-dir={profile_root}'")
            options.add_argument(f'--user-data-dir={profile_root}')
        chrome_profile = settings.get('CHROME_PROFILE')
        if chrome_profile:
            logger.info(f"Adding Chrome option "
                        f"'--profile-directory={chrome_profile}'")
            options.add_argument(f'--profile-directory={chrome_profile}')
        chrome_binary = settings.get('CHROME_BINARY')
        if chrome_binary:
            logger.info(f"Adding Chrome option "
                        f"'binary_location': {chrome_binary}")
            options.binary_location = os.path.expanduser(chrome_binary)
        service_args = settings.get('CHROME_DRIVER_SERVICE_ARGS')
        if service_args:
            for arg in service_args:
                logger.info(f'Adding chromedriver service arg: {arg}')
        driver = webdriver.Chrome(
            chrome_options=options,
            executable_path=settings['CHROME_DRIVER_PATH'],
            service_args=service_args
        )
        if settings['SYSTEM'] == 'Darwin':
            time.sleep(1)
            focus_browser()
    else:
        logger.critical(f'Test harness does not currently support the '
                        f'{browser} browser.')
        sys.exit(3)
    return driver


def _get_field(fieldable):
    """called by enter_value to allow clients to invoke an object or a
       function
    """
    if callable(fieldable):
        return fieldable()
    else:
        return fieldable


def enter_value(fieldable, value):
    """enter value into form field"""
    for i in range(5):
        # for when we loop and search more than once
        _get_field(fieldable).clear()
        _get_field(fieldable).send_keys(value)
        # We sleep here because Selenium trips over itself and enters values
        # into the wrong form field if it tries to fill-out the form
        # too quickly.
        time.sleep(0.5)
        actual_text = _get_field(fieldable).get_attribute('value')
        if actual_text == value:
            break
        else:
            time.sleep(0.5)

        logger.warning(f'\nWrong value entered for '
                       f'{repr(_get_field(fieldable))}: \n'
                       f'Actual Text: {repr(actual_text)} \n'
                       f'Value Given: {repr(value)}')
    else:
        logger.error(f'There was a problem filling in the '
                     f'{_get_field(fieldable)} field.')


def load_data(yaml_file):
    data_file = os.path.join(DATA_PATH, yaml_file)
    with open(data_file, 'r', encoding='utf-8') as stream:
        data = yaml.load(stream)

    return data


def weighted_choice(choices, weights):
    total = sum(weights)
    threshold = random.uniform(0, total)
    for k, weight in enumerate(weights):
        total -= weight
        if total < threshold:
            return choices[k]


def reduce_time(seconds):
    # 86400 seconds/day
    if seconds >= 86400:
        days = floor(seconds / 86400)
        return [days, 'days']
    # 3600 seconds/hour
    if 86400 > seconds >= 3600:
        hours = floor(seconds / 3600)
        return [hours, 'hours']
    # 60 seconds/minute
    else:
        minutes = floor(seconds / 60)
        return [minutes, 'minutes']


def time_str2secs(time_str: str) -> int:
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)


def seconds_to_time_duration_string(seconds):
    seconds = int(seconds)
    # These are for convenience
    minute = 60
    hour = (minute ** 2)
    day = (hour * 24)
    week = (day * 7)
    month = (week * 4)
    year = (month * 12)

    secs, mins, hrs, days, weeks, months, years = 0, 0, 0, 0, 0, 0, 0

    if seconds >= year:
        years = (seconds // year)
        tmp = (seconds % year)
        seconds = tmp
    if seconds >= month:
        months = (seconds // month)
        tmp = (seconds % month)
        seconds = tmp
    if seconds >= week:
        weeks = (seconds // week)
        tmp = (seconds % week)
        seconds = tmp
    if seconds >= day:
        days = (seconds // day)
        tmp = (seconds % day)
        seconds = tmp
    if seconds >= hour:
        hrs = (seconds // hour)
        tmp = (seconds % hour)
        seconds = tmp
    if seconds >= minute:
        mins = (seconds // minute)
        secs = (seconds % minute)
    if seconds < minute:
        secs = seconds

    time_str = ''

    if years != 0:
        time_str += f'{years}yr '
    if months != 0:
        time_str += f'{months}mo '
    if weeks != 0:
        time_str += f'{weeks}wk '
    if days != 0:
        time_str += f'{days}d '
    if hrs != 0:
        time_str += f'{hrs}h '
    if mins != 0:
        time_str += f'{mins}m '
    if secs != 0:
        time_str += f'{secs}s '

    return time_str.strip()


def dd2dms(dd, measure):
    dd = float(dd)
    direction = ''
    if measure == 'latitude':
        if dd > 0:
            direction = 'N'
        elif dd < 0:
            direction = 'S'
    if measure == 'longitude':
        if dd > 0:
            direction = 'E'
        elif dd < 0:
            direction = 'W'

    dd = abs(dd)
    degrees = int(dd)
    min_sec = (dd - degrees) * 60
    minutes = int(min_sec)
    sec = min_sec % minutes
    seconds = round((sec * 60), 4)

    dms = f"{degrees}°{minutes}'{seconds}\"{direction}"

    return dms


def truncate(number, places):
    if not isinstance(places, int):
        raise ValueError("Decimal places must be an integer.")
    if places < 1:
        raise ValueError("Decimal places must be at least 1.")
    # If you want to truncate to 0 decimal places, just do int(number).

    with localcontext() as context:
        context.rounding = ROUND_DOWN
        exponent = Decimal(str(10 ** - places))
        return Decimal(str(number)).quantize(exponent).to_eng_string()


js_action = \
    """
    function harnessGetElementByXpath(path) {
      return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    }

    function harnessActionExec(exPath, action) {
        if (action=='click') {
            harnessGetElementByXpath(exPath).click()
        } else if (action=='scroll') {
            harnessGetElementByXpath(exPath).scrollIntoView(false)
        } else {
            console.log("harnessActionExec: Unknown Action: " + action)
        }
    }

    var exPath = arguments[0]
    var harnessAction = arguments[1]
    harnessActionExec(exPath, harnessAction)
    """


def __check_condition_parameter_is_function(condition):
    # Check the condition is a callable lambda or function.
    if not hasattr(condition, '__call__'):
        raise TypeError(("Condition argument does not appear to be a "
                         "callable function.",
                         "Please check if this is a properly formatted "
                         "lambda/function statement."),
                        condition
                        )


class OperationTimeoutError(Exception):
    """
    Timeout Error
    This error is thrown when a wait function times out.
    """
    pass


# noinspection PyPep8Naming
def smallTalk_case(word):
    p = re.search('_', word)
    if p:
        proto = ''.join(w.capitalize() for w in word.split('_'))
    else:
        proto = word
    m = re.search('[a-z]+', proto)
    if not m:
        return proto.lower()
    p = m.start(0)
    return proto[:p].lower() + proto[p:]


def ends_with(string, ending):
    """ This is a workaround for implementing the XPath 2.0 ends-with
    function in XPath 1.0.

    """
    return (f'substring({string}, string-length({string}) - '
            f'string-length("{ending}") +1) = "{ending}"')


def run_db_query(query, *args):
    """
    Run query provided, passing in query parameters provided by *args
    This is a convenience function that will work for many basic queries, but
    will *not* support all SQL syntax.
    For instance, it does not support SQL's 'IN' keyword. If you need to query
    for multiple values, you will need to call this function in a `for` loop,
    or roll your own...

    Args:
        :param query:
        :param args: An argument list. Arguments in this list are parameters
                     to the query being run and should appear in the order
                     that they are used for interpolation in the query.
    """
    results = []
    if settings['MODE'] in ['dev', 'ci']:
        cnx = mysql.connector.connect(
            user=settings['DB_SYSTEM_USER'],
            password=settings['DB_SYSTEM_PASS'],
            host='127.0.0.1',
            database=settings['DB_SYSTEM_NAME'],
            port=settings['HOST_DB_PORT'])
        cursor = cnx.cursor(named_tuple=True)
        if args:
            cursor.execute(query.format(*args))
        else:
            cursor.execute(query)
        for row in cursor:
            results.append(row)
        cursor.close()
        cnx.close()
    else:
        with SSHTunnelForwarder(
                (settings['HOST_DB_NAME'], 22),
                ssh_username=settings['HOST_DB_SSH_USER'],
                ssh_pkey=settings['HOST_DB_KEYFILE'],
                ssh_password=settings['HOST_DB_SSH_PASS'],
                remote_bind_address=('127.0.0.1', settings['HOST_DB_PORT']),
                local_bind_address=('127.0.0.1', 53306)
        ):
            cnx = mysql.connector.connect(
                user=settings['DB_SYSTEM_USER'],
                password=settings['DB_SYSTEM_PASS'],
                host='127.0.0.1',
                database=settings['DB_SYSTEM_NAME'],
                port=53306
            )
            cursor = cnx.cursor(named_tuple=True)
            if args:
                cursor.execute(query.format(*args))
            else:
                cursor.execute(query)
            for row in cursor:
                results.append(row)
            cursor.close()
            cnx.close()
    return results


def focus_browser(browser="Google Chrome"):
    script = f"""
        tell application "{browser}"
           activate
           delay 1
        end tell"""

    osa = subprocess.Popen(
        ['osascript', '-'],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    osa.communicate(dedent(script).encode('utf-8'))


def get_test_resource(
    prefix: str,
    test_data_resource_uri: url_parse.SplitResult,
    expect_query: bool = True
) -> Dict:
    """

    :param prefix: assert report prefix
    :param test_data_resource_uri: urllib.parse.SplitResult
    :param expect_query: Boolean
    :return: dictionary having keys for the file_name and named parameters
    """
    assert isinstance(test_data_resource_uri, url_parse.SplitResult) and \
        test_data_resource_uri, \
        (f'{prefix} test_data_resource_uri is not a SplitResult or is empty: '
         f'{test_data_resource_uri}')

    assert not test_data_resource_uri.scheme or \
        test_data_resource_uri.scheme == 'file', \
        (f'{prefix} test_data_resource_uri {test_data_resource_uri}\n\t'
         f'has unsupported scheme: {test_data_resource_uri.scheme}')

    assert test_data_resource_uri.hostname == 'localhost', \
        (f'{prefix} test_data_resource_uri {test_data_resource_uri}\n\t'
         f'has unsupported hostname: {test_data_resource_uri.hostname}')

    assert test_data_resource_uri.scheme == 'resource' or \
        test_data_resource_uri.path, \
        (f'{prefix} test_data_resource_uri {test_data_resource_uri}\n\t'
         f'has no path: {test_data_resource_uri.path}')

    path = test_data_resource_uri.path
    resource = {}

    if expect_query:
        assert test_data_resource_uri.query, \
            f'{prefix} test_data_resource_uri {test_data_resource_uri}\n\t' \
            f'has no query params: {test_data_resource_uri.query}'
        resource.update(url_parse.parse_qs(test_data_resource_uri.query))

    if path:
        resource['file_name'] = path if path[0] != '/' else path[1:]

    return resource
