tell application "iTerm"
	activate
	if (count of window with tab) = 0 then
		set term to (make new terminal)
	else
		set term to current window
	end if
	tell term
		set new_tab to (create tab with profile "BTF")
	end tell
	tell current session of new_tab
		write text "ll"
	end tell
end tell
