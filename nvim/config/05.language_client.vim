" language_client.vim contains vim settings specific to the language
" client plugin

" language server commands
let g:LanguageClient_serverCommands = {
            \ 'cpp': ['ccls'],
            \ 'c': ['ccls'],
            \ 'python': ['/home/btf/.local/bin/pyls'],
            \ }
let g:LanguageClient_autoStart = 1
let g:LanguageClient_rootMarkers = {
            \ 'cpp': ['compile_commands.json', 'build'],
            \ 'c': ['compile_commands.json', 'build'],
            \ }

set completefunc=LanguageClient#complete

let g:LanguageClient_loadSettings = 1
let g:LanguageClient_settingsPath = '/home/afnan/.config/nvim/settings.json'

