" Required:
set runtimepath+=/Users/btf/.local/share/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('/Users/btf/.local/share/dein')
    call dein#begin('/Users/btf/.local/share/dein')

    " Let dein manage dein
    " Required:
    call dein#add('/Users/btf/.local/share/dein/repos/github.com/Shougo/dein.vim')

    " Add or remove your plugins here:
    call dein#add('Shougo/neosnippet')
    call dein#add('Shougo/neosnippet-snippets')
    call dein#add('sheerun/vim-polyglot')
    call dein#add('Shougo/vimshell')
    call dein#add('Shougo/deoplete.nvim')
    call dein#add('deoplete-plugins/deoplete-jedi')
    call dein#add('vim-airline/vim-airline')
    call dein#add('vim-airline/vim-airline-themes')
    call dein#add('Shougo/neoinclude.vim')
    call dein#add('Shougo/neco-vim')
    call dein#add('scrooloose/nerdtree')
    call dein#add('scrooloose/nerdcommenter')
    call dein#add('icymind/NeoSolarized')
    call dein#add('wmvanvliet/vim-blackboard')
    call dein#add('tyrannicaltoucan/vim-deep-space')
    call dein#add('xypnox/OceanicMaterialVim')
    call dein#add('mhartington/oceanic-next')
    call dein#add('tmhedberg/SimpylFold')
    call dein#add('Konfekt/FastFold')
    call dein#add('fatih/vim-go')
    call dein#add('w0rp/ale')
    call dein#add('Shougo/neoinclude.vim')
    call dein#add('cloudhead/neovim-fuzzy')
    call dein#add('neomutt/neomutt.vim')
    call dein#add('christoomey/vim-tmux-navigator')
    call dein#add('jamessan/vim-gnupg')
    call dein#add('hashivim/vim-hashicorp-tools')
    call dein#add('ryanoasis/vim-devicons')
    call dein#add('tiagofumo/vim-nerdtree-syntax-highlight')
    call dein#add('autozimu/LanguageClient-neovim', {
                \ 'rev': 'next',
                \ 'build': 'bash install.sh',
                \ })
    " Required:
    call dein#end()
    call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
    call dein#install()
endif

"End dein Scripts-------------------------

