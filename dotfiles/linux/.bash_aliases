
# Make direnv easier
alias da='direnv allow'

# Various flavors of ls
alias ls='ls -aF --color --group-directories-first'
alias ll='ls -aFl --color --group-directories-first'
alias lb='ls -aFlH --color --group-directories-first'

# Grep history list
alias hgrep="history | cut -d'|' -f 3 | sed 's/^ *//g' | grep $1"

# Prefer Neovim
alias vi='nvim'
alias vim='nvim'

# Make grep more user friendly by highlighting matches
alias grep='grep --color=auto'

# Show Process Inheritance Tree with cgroup info
alias psc='ps xawf -eo pid,user,cgroup,args'

# Get a real login shell when you su
alias su='su -'

# Show local primary IP address
alias localip='ifconfig | sed -rn "s/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p"'
