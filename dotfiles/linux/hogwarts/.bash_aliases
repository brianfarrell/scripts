
# Various flavors of ls
alias ls='ls -aF --color --group-directories-first'
alias ll='ls -aFl --color --group-directories-first'
alias lb='ls -aFlH --color --group-directories-first'

# Grep history list
alias hgrep="history | grep $1"

# Prefer Neovim
alias vi='nvim'
alias vim='nvim'

# Make grep more user friendly by highlighting matches
alias grep='grep --color=auto'

# Show Process Inheritance Tree with cgroup info
alias psc='ps xawf -eo pid,user,cgroup,args'
