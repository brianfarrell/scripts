# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Set the path here
PATH=$PATH:$HOME/.local/bin

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

# Add bash aliases
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

# Set some sensible, modern defaults
export LANG=en_US.UTF-8
export LC_COLLATE=C

# Set editor to vim
export EDITOR='/usr/bin/nvim'
export GIT_EDITOR='/usr/bin/nvim'

# Set up for python's virtualenvwrapper
export WORKON_HOME=$HOME/.venv
export PROJECT_HOME=$HOME/projects
source /usr/bin/virtualenvwrapper-3.sh

