
# Add bash aliases
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

# Set some sensible, modern defaults
export LANG=en_US.UTF-8
export LC_COLLATE=C
export LC_CTYPE=C

# Setup XDG Environment
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share

# Set editor to NeoVim
export EDITOR='nvim'
export GIT_EDITOR='nvim'

# Increase length of Command History
HISTSIZE=2500
HISTFILESIZE=5000

# Add timestamp to Command History items
export HISTTIMEFORMAT='| %F %T | '

# Enable direnv
eval "$(direnv hook bash)"

# Set up for python's virtualenvwrapper
export WORKON_HOME=$HOME/.venv
export PROJECT_HOME=$HOME/Projects
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/bin/virtualenv
source /usr/bin/virtualenvwrapper.sh

# Functions for Docker
export DOCKER_COMPLETION_SHOW_CONFIG_IDS=yes
export DOCKER_COMPLETION_SHOW_CONTAINER_IDS=yes
export DOCKER_COMPLETION_SHOW_NETWORK_IDS=yes
export DOCKER_COMPLETION_SHOW_NODE_IDS=yes
export DOCKER_COMPLETION_SHOW_PLUGIN_IDS=yes
export DOCKER_COMPLETION_SHOW_SECRET_IDS=yes
export DOCKER_COMPLETION_SHOW_SERVICE_IDS=yes
export DOCKER_COMPLETION_SHOW_IMAGE_IDS=non-intermediate
export DOCKER_COMPLETION_SHOW_TAGS=yes

# test if the prompt var is not set and also to prevent failures
# when `$PS1` is unset and `set -u` is used 
if [ -z "${PS1:-}" ]; then
    # prompt var is not set, so this is *not* an interactive shell
    return
fi

# Show me colors!
export TERM="xterm-256color"
export CLICOLOR=1
export NVIM_TUI_ENABLE_TRUE_COLOR=1

# If we reach this line of code, then the prompt var is set, so
# this is an interactive shell.

# Add colors to man pages
export LESS_TERMCAP_mb=$(tput bold; tput setaf 1) # red
export LESS_TERMCAP_md=$(tput bold; tput setaf 2) # green
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 3) # yellow
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput smul; tput setaf 6) # cyan
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
export LESS_TERMCAP_ZN=$(tput ssubm)
export LESS_TERMCAP_ZV=$(tput rsubm)
export LESS_TERMCAP_ZO=$(tput ssupm)
export LESS_TERMCAP_ZW=$(tput rsupm)
export GROFF_NO_SGR=1         # For Konsole and Gnome-terminal

# When displaying man pages, display percentage into the document
export MANPAGER='less -s -M +Gg'
