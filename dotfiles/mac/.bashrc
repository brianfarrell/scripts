# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Add bash aliases
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

# After each command, append to the history file and reread it
export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'n'}history -a; history -c; history -r"

# Stop Homebrew from automatically running cleanup!
export HOMEBREW_NO_INSTALL_CLEANUP=true

# Set editor to NeoVim
export EDITOR='subl'
export GIT_EDITOR='nvim'

# Set the PS1  prompt
PS1='[\u@\h \W]\$ '

# Show me colors!
export TERM="xterm-256color"
export CLICOLOR=1
export NVIM_TUI_ENABLE_TRUE_COLOR=1
LS_COLORS='rs=0:di=38;5;33:ln=38;5;51:mh=00:pi=40;38;5;11:so=38;5;13:do=38;5;5:bd=48;5;232;38;5;11:cd=48;5;232;38;5;3:or=48;5;232;38;5;9:mi=01;05;37;41:su=48;5;196;38;5;15:sg=48;5;11;38;5;16:ca=48;5;196;38;5;226:tw=48;5;10;38;5;16:ow=48;5;10;38;5;21:st=48;5;21;38;5;15:ex=38;5;40:*.tar=38;5;9:*.tgz=38;5;9:*.arc=38;5;9:*.arj=38;5;9:*.taz=38;5;9:*.lha=38;5;9:*.lz4=38;5;9:*.lzh=38;5;9:*.lzma=38;5;9:*.tlz=38;5;9:*.txz=38;5;9:*.tzo=38;5;9:*.t7z=38;5;9:*.zip=38;5;9:*.z=38;5;9:*.dz=38;5;9:*.gz=38;5;9:*.lrz=38;5;9:*.lz=38;5;9:*.lzo=38;5;9:*.xz=38;5;9:*.zst=38;5;9:*.tzst=38;5;9:*.bz2=38;5;9:*.bz=38;5;9:*.tbz=38;5;9:*.tbz2=38;5;9:*.tz=38;5;9:*.deb=38;5;9:*.rpm=38;5;9:*.jar=38;5;9:*.war=38;5;9:*.ear=38;5;9:*.sar=38;5;9:*.rar=38;5;9:*.alz=38;5;9:*.ace=38;5;9:*.zoo=38;5;9:*.cpio=38;5;9:*.7z=38;5;9:*.rz=38;5;9:*.cab=38;5;9:*.wim=38;5;9:*.swm=38;5;9:*.dwm=38;5;9:*.esd=38;5;9:*.jpg=38;5;13:*.jpeg=38;5;13:*.mjpg=38;5;13:*.mjpeg=38;5;13:*.gif=38;5;13:*.bmp=38;5;13:*.pbm=38;5;13:*.pgm=38;5;13:*.ppm=38;5;13:*.tga=38;5;13:*.xbm=38;5;13:*.xpm=38;5;13:*.tif=38;5;13:*.tiff=38;5;13:*.png=38;5;13:*.svg=38;5;13:*.svgz=38;5;13:*.mng=38;5;13:*.pcx=38;5;13:*.mov=38;5;13:*.mpg=38;5;13:*.mpeg=38;5;13:*.m2v=38;5;13:*.mkv=38;5;13:*.webm=38;5;13:*.ogm=38;5;13:*.mp4=38;5;13:*.m4v=38;5;13:*.mp4v=38;5;13:*.vob=38;5;13:*.qt=38;5;13:*.nuv=38;5;13:*.wmv=38;5;13:*.asf=38;5;13:*.rm=38;5;13:*.rmvb=38;5;13:*.flc=38;5;13:*.avi=38;5;13:*.fli=38;5;13:*.flv=38;5;13:*.gl=38;5;13:*.dl=38;5;13:*.xcf=38;5;13:*.xwd=38;5;13:*.yuv=38;5;13:*.cgm=38;5;13:*.emf=38;5;13:*.ogv=38;5;13:*.ogx=38;5;13:*.aac=38;5;45:*.au=38;5;45:*.flac=38;5;45:*.m4a=38;5;45:*.mid=38;5;45:*.midi=38;5;45:*.mka=38;5;45:*.mp3=38;5;45:*.mpc=38;5;45:*.ogg=38;5;45:*.ra=38;5;45:*.wav=38;5;45:*.oga=38;5;45:*.opus=38;5;45:*.spx=38;5;45:*.xspf=38;5;45:';
export LS_COLORS

# Run on new terminal
if [ -z ${NO_FORTUNE+x} ]; then
    fortune
fi

# Increase length of Command History
HISTSIZE=2500
HISTFILESIZE=5000

# Add timestamp to Command History items
export HISTTIMEFORMAT='| %F %T | '

# Set some sensible, modern defaults
export LANG=en_US.UTF-8
export LC_COLLATE=C
export LC_CTYPE=C

# Add newer sqlite3 from Homebrew to PATH
export PATH="/usr/local/opt/sqlite/bin:$PATH"

# Add local bin directory to PATH
export PATH="/Users/btf/.local/bin:$PATH"

# Ansible config
export ANSIBLE_CONFIG=$HOME/.local/etc/ansible/ansible.cfg

# Pipenv config
export PIPENV_VERBOSITY=-1
export PIPENV_DONT_USE_ASDF=1
export PIPENV_DONT_USE_PYENV=1
eval "$(pipenv --completion)"

# Set up for python's virtualenvwrapper
export WORKON_HOME=$HOME/.venv
export PROJECT_HOME=$HOME/Projects
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/bin/virtualenvwrapper.sh

# Do this so psycopg2 works for Python/PostgreSQL (b/c psycopg2 is a piece of shit):
# export DYLD_FALLBACK_LIBRARY_PATH=/Library/PostgreSQL/9.5/lib:$DYLD_FALLBACK_LIBRARY_PATH

# Remove executable bit if not a directory
function noex () {
    for f in $1/* $1/.[^.]*; do
        if [ ! -d "$f" ]; then
            chmod 0664 "$f"
        fi
    done
}

# Vagrant
export VAGRANT_DEFAULT_PROVIDER=vmware_desktop

# Functions for Docker
export DOCKER_COMPLETION_SHOW_CONFIG_IDS=yes
export DOCKER_COMPLETION_SHOW_CONTAINER_IDS=yes
export DOCKER_COMPLETION_SHOW_NETWORK_IDS=yes
export DOCKER_COMPLETION_SHOW_NODE_IDS=yes
export DOCKER_COMPLETION_SHOW_PLUGIN_IDS=yes
export DOCKER_COMPLETION_SHOW_SECRET_IDS=yes
export DOCKER_COMPLETION_SHOW_SERVICE_IDS=yes
export DOCKER_COMPLETION_SHOW_IMAGE_IDS=non-intermediate
export DOCKER_COMPLETION_SHOW_TAGS=yes

function dsh () {
    docker exec -it "${1}" /bin/bash
}


complete -C /usr/local/bin/vault vault

complete -C /usr/local/bin/consul consul

# Add direnv hook
eval "$(direnv hook bash)"


CONDA_ROOT=~/.local/anaconda3   # <- set to your Anaconda/Miniconda installation directory
if [[ -r $CONDA_ROOT/etc/profile.d/bash_completion.sh ]]; then
    source $CONDA_ROOT/etc/profile.d/bash_completion.sh
fi

# ----------------------------------------------------------------------------
# A function to create a compressed tarball of a directory in the current
# folder, if there's sufficient space
# ----------------------------------------------------------------------------
targz() {
  d="${1}"
  if [ -d "${d}" ]
  then
    if [ $(stat -f --printf="%a * %s / 1024\n" . | bc) -gt $(du -sk ./"${d}" | awk '{print $1}') ]
    then
      tar cvfz "${d}.tgz" "${d}"
    else
      echo "Low space in $(pwd)"
    fi
  else
    echo "Can't access ${d}"
  fi
}

# ----------------------------------------------------------------------------
# The following function would rsync stuff from the specified source to the
# target with all the common and useful options.
# ----------------------------------------------------------------------------
myrsync() {
  s="${1}"
  t="${2}"
  if [ -d "${s}" ]
  then
    if [ ! -d "${t}" ]
    then
      echo "Creating target ${t}"
      mkdir -p "${t}"
    fi
    echo "Checking the size of ${s}"
    if [ $(du -sk "${s}" | awk '{print $1}') -lt \
    $(df -k "${t}" | sed 's/ \+/ /g' | grep -oP "(?<= )[0-9]{1,}(?= [0-9]{2}%)") ]
    then
      rsync -avuKxh --progress --log-file="$(mktemp)" "${s}" "${t}"
    else
      echo "Low space in ${t}"
    fi
  else
    echo "Can't access ${s}"
  fi
}

# ----------------------------------------------------------------------------
# Extract an archive file by running the correct command base on the
# filename extension
# ----------------------------------------------------------------------------
extract () {
  if [ -f "${1}" ] ; then
    case $1 in
      *.tar.bz2)  tar xjf    "${1}"    ;;
      *.tar.gz)   tar xzf    "${1}"    ;;
      *.bz2)      bunzip2    "${1}"    ;;
      *.rar)      rar x      "${1}"    ;;
      *.gz)       gunzip     "${1}"    ;;
      *.tar)      tar xf     "${1}"    ;;
      *.tbz2)     tar xjf    "${1}"    ;;
      *.tgz)      tar xzf    "${1}"    ;;
      *.zip)      unzip      "${1}"    ;;
      *.Z)        uncompress "${1}"    ;;
      *)      echo "Unknown file type" ;;
    esac
  else
    echo "Can't access ${1}"
  fi
}
