
# Various flavors of ls
alias ls='gls -aF --color --group-directories-first'
alias ll='gls -aFl --color --group-directories-first'
alias lb='gls -aFlh --color --group-directories-first'

# Make grep more user friendly by highlighting matches
alias grep='grep --color=auto'

# Grep history list
alias hgrep="history | grep $1"

# Prefer Neovim
alias vi='nvim'
alias vim='nvim'

# Get a real login shell when you su
alias su='su -'

# Docker Workflow
alias dc='docker-compose'
alias dm='docker-machine'

# Show formatted `docker ps`
alias dps='docker ps --format "table {{.ID}}\t{{.Status}}\t{{.Names}}\t{{.Mounts}}\t{{.Ports}}"'

# Alternates
alias rpn='/usr/bin/dc'

